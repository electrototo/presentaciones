package com.smt.persistencia

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun createFile(button: View) {
        val intent = Intent(this, CreateFileActivity::class.java)
        startActivity(intent)
    }

    fun listFiles(button: View) {
        val intent = Intent(this, ListFilesActivity::class.java)
        startActivity(intent)
    }

    fun deleteFiles(button: View) {
        // TODO 4.1 borrar todos los archivos, tanto cache como estándar
    }

    fun querySpace(button: View) {
        // TODO 5.1 pedir al usuario que borre archivos para adquirir más espacio
    }
}