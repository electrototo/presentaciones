package com.smt.persistencia

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Spinner
import com.smt.persistencia.enums.FileType
import java.io.File

class CreateFileActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    private lateinit var filenameField: EditText
    private lateinit var contentField: EditText

    private lateinit var fileTypeSpinner: Spinner
    private lateinit var adapter: ArrayAdapter<String>

    private var modes: List<String> = listOf("Choose a file type...", "Standard", "Cache")

    private var selectedType: FileType = FileType.UNKNOWN

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_file)

        adapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, modes)

        fileTypeSpinner = findViewById(R.id.fileTypeSpinner)
        fileTypeSpinner.adapter = adapter
        fileTypeSpinner.onItemSelectedListener = this

        filenameField = findViewById(R.id.filename)
        contentField = findViewById(R.id.content)
    }

    fun saveFile(button: View) {
        val filename = filenameField.text.toString()
        val content = contentField.text.toString()

        var file: File? = null;

        if (filename.isNotBlank() && content.isNotBlank()) {
            // TODO: 1.1 Almacenar el archivo con el nombre y el contenido especificado
        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        // TODO: 3.1 Guardar el archivo seleccionado de acuerdo al modo especificado
    }
}