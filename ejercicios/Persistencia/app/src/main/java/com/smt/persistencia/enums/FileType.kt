package com.smt.persistencia.enums

enum class FileType {
    UNKNOWN, STANDARD, CACHE
}