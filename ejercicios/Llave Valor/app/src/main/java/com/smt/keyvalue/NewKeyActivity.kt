package com.smt.keyvalue

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText

// TODO 3: Reto. Consulta la documentacion de getSharedPreferences y guarda el valor en la llave
//         especificada con la llave de SharedPreferences "ejercicio.sharedPreferences

class NewKeyActivity : AppCompatActivity() {

    private lateinit var keyEditText: EditText
    private lateinit var valueEditText: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_key)

        keyEditText = findViewById(R.id.keyEditText)
        valueEditText = findViewById(R.id.valueEditText)
    }

    fun saveLocally(view: View) {
        // TODO 1.1: Guarda en SharedPreferences local los valores - llave.
    }

    fun saveGlobally(view: View) {
        // TODO 1.2: Guarda globalmente la llave-valor
    }
}