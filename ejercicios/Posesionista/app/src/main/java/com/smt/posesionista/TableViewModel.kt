package com.smt.posesionista

import androidx.lifecycle.ViewModel
import kotlin.random.Random

class TableViewModel : ViewModel() {
    val inventory = mutableListOf<Thing>()
    val items = arrayOf("Book 1", "Book 2", "Map", "Compass", "Hiking shoes")

    init {
        for (i in 0..99) {
            inventory.add(Thing(name=items[Random.nextInt(items.size)], price=Random.nextInt(10,200)))
        }
    }

    fun remove(pos: Int) = inventory.removeAt(pos)

    // TODO 3: Implementación de la escritura del contenido del modelo

    // TODO 5: Implementación de la lectura del contenido del modelo
}