package com.smt.posesionista

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.text.NumberFormat
import java.util.*

class TableFragment : Fragment() {

    interface Callback {
        fun onThingSelected(thing: Thing)
    }

    companion object {
        fun newInstance() = TableFragment()

        // TODO 2.1: Especificar el request code de la actividad de escritura
        // TODO 4.1: Especificar el request code de la actividad de lectura
    }

    private var callback: Callback? = null
    private lateinit var viewModel: TableViewModel

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    private lateinit var saveButton: Button
    private lateinit var loadButton: Button

    override fun onAttach(context: Context) {
        super.onAttach(context)
        callback = context as Callback?
    }

    override fun onDetach() {
        super.onDetach()
        callback = null
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.table_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        saveButton = view!!.findViewById(R.id.saveDataButton)
        saveButton.setOnClickListener {
            this.saveData(it)
        }

        loadButton = view!!.findViewById(R.id.loadDataButton)
        loadButton.setOnClickListener {
            this.loadData(it)
        }

        viewModel = ViewModelProvider(this).get(TableViewModel::class.java)

        viewManager = LinearLayoutManager(context)
        viewAdapter = ThingAdapter()

        recyclerView = view!!.findViewById<RecyclerView>(R.id.recyclerView).apply {
            layoutManager = viewManager
            adapter = viewAdapter
        }

        configTouchHelper()
    }

    fun saveData(view: View) {
        // TODO 2: Crear una actividad para seleccionar la ubicación del archivo
    }

    // TODO 2.2: Esperar el resultado de la actividad de escritura
    // TODO 4.2: Esperar el resultado de la actividad de lectura

    fun loadData(view: View) {
        // TODO 4: Crear una actividad para seleccionar la unicacion del archivo a leer
    }

    private inner class ThingAdapter: RecyclerView.Adapter<ThingAdapter.ThingViewHolder>() {

        private inner class ThingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
            val thingName = itemView.findViewById<TextView>(R.id.thingName)
            val thingPrice = itemView.findViewById<TextView>(R.id.thingPrice)

            lateinit var thing: Thing

            init {
                itemView.setOnClickListener(this)
            }

            fun bind(thing: Thing) {
                this.thing = thing

                val format = NumberFormat.getInstance().apply {
                    maximumFractionDigits = 2
                    currency = Currency.getInstance("USD")
                }

                thingName.text = thing.name
                thingPrice.text = format.format(thing.price)
            }

            override fun onClick(view: View?) {
                callback?.onThingSelected(thing)
            }
        }

        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): ThingAdapter.ThingViewHolder {
            val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.thing_item, parent, false)

            return ThingViewHolder(itemView)
        }

        override fun getItemCount() = viewModel.inventory.size

        override fun onBindViewHolder(holder: ThingAdapter.ThingViewHolder, position: Int) {
            val thing = viewModel.inventory[position]
            holder.bind(thing)
        }
    }
    private fun configTouchHelper() {
        val itemTouchCallback = object: ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                TODO("Not yet implemented")
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                if (direction == ItemTouchHelper.LEFT) {
                    viewModel.remove(viewHolder.adapterPosition)
                    recyclerView.adapter?.notifyItemRemoved(viewHolder.adapterPosition)
                }
            }
        }

        val gestureDetector = ItemTouchHelper(itemTouchCallback)
        gestureDetector.attachToRecyclerView(recyclerView)
    }
}