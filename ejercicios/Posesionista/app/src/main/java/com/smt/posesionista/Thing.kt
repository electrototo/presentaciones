package com.smt.posesionista

import java.io.Serializable
import java.util.*

data class Thing(
    var name : String = "",
    var price : Int = 0,
    val id : UUID = UUID.randomUUID(),
    val creationDate : Date = Date()
) {
}