package com.smt.keyvalue

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import androidx.preference.PreferenceManager

class ListKeysActivity : AppCompatActivity() {

    private lateinit var valueContent: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_keys)

        valueContent = findViewById(R.id.valueContent)

        // TODO 2.1: Enlista todas las llaves almacenadas
        val allData = PreferenceManager.getDefaultSharedPreferences(applicationContext).all.entries.fold("") { accum, (key, value) ->
            "$key: $value\n$accum"
        }

        valueContent.text = allData
    }
}