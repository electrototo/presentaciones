package com.smt.keyvalue

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun newKey(view: View) {
        val intent = Intent(this, NewKeyActivity::class.java)
        startActivity(intent)
    }

    fun listKeys(view: View) {
        val intent = Intent(this, ListKeysActivity::class.java)
        startActivity(intent)
    }
}