package com.smt.keyvalue

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import androidx.preference.PreferenceManager

// TODO 3: Reto. Consulta la documentacion de getSharedPreferences y guarda el valor en la llave
//         especificada con la llave de SharedPreferences "ejercicio.sharedPreferences

class NewKeyActivity : AppCompatActivity() {

    private lateinit var keyEditText: EditText
    private lateinit var valueEditText: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_key)

        keyEditText = findViewById(R.id.keyEditText)
        valueEditText = findViewById(R.id.valueEditText)
    }

    fun saveLocally(view: View) {
        val key = keyEditText.text.toString()
        val value = valueEditText.text.toString()

        val prefs = getPreferences(Context.MODE_PRIVATE)

        // TODO 1.1: Guarda en SharedPreferences local los valores - llave.
        if (key.isNotBlank() && value.isNotBlank()) {
            with (prefs.edit()) {
                putString(key, value)
                commit()
            }

            finish()
        }
    }

    fun saveGlobally(view: View) {
        val key = keyEditText.text.toString()
        val value = valueEditText.text.toString()

        val prefs = PreferenceManager.getDefaultSharedPreferences(applicationContext)

        // TODO 1.2: Guarda globalmente la llave-valor
        if (keyEditText.text.toString().isNotBlank() && valueEditText.text.toString().isNotBlank()) {
            with (prefs.edit()) {
                putString(key, value)
                commit()
            }

            finish()
        }
    }
}