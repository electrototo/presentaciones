package com.smt.persistencia

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import com.smt.persistencia.enums.FileType
import java.io.File
import java.io.FileInputStream
import java.nio.charset.Charset

class ListFilesActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    private lateinit var fileChooser: Spinner
    private var adapter: ArrayAdapter<String>? = null

    private lateinit var fileContents: TextView

    private var files: MutableList<String> = mutableListOf()

    // Código parte 3
    private var selectedType = FileType.STANDARD
    // Finaliza código parte 3

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_files)

        listStandardFiles(null)

        adapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, files)

        fileChooser = findViewById(R.id.fileChooser)
        fileChooser.adapter = adapter
        fileChooser.onItemSelectedListener = this

        fileContents = findViewById(R.id.fileContents)
    }

    fun listStandardFiles(view: View?) {
        // TODO 2.1 Cargar los nombres de los archivos en el directorio a la lista *files*
        files.clear()

        applicationContext.fileList().forEach { file: String ->
            files.add(file)
        }

        adapter?.notifyDataSetChanged()
        selectedType = FileType.STANDARD
    }

    fun listCacheFiles(view: View?) {
        // TODO 3.1 Cargar los nombres de los archivos del directorio cache a la lista *file*
        files.clear()

        applicationContext.cacheDir.listFiles()?.forEach { file: File ->
            files.add(file.name)
        }

        adapter?.notifyDataSetChanged()
        selectedType = FileType.CACHE
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        var file: File? = null

        if (selectedType == FileType.STANDARD) {
            file = File(applicationContext.filesDir, files[position])
        }
        else if (selectedType == FileType.CACHE) {
            file = File(applicationContext.cacheDir, files[position])
        }

        // TODO 2.2 Mostrar los contenidos del archivo seleccionado en el view fileContents
        FileInputStream(file).use {
            fileContents.text = it.readBytes().toString(Charset.defaultCharset())
        }
    }
}