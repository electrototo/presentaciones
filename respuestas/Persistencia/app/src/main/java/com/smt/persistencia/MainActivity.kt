package com.smt.persistencia

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.storage.StorageManager
import android.util.Log
import android.view.View

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun createFile(button: View) {
        val intent = Intent(this, CreateFileActivity::class.java)
        startActivity(intent)
    }

    fun listFiles(button: View) {
        val intent = Intent(this, ListFilesActivity::class.java)
        startActivity(intent)
    }

    fun deleteFiles(button: View) {
        // TODO 4.1 borrar todos los archivos, tanto cache como estándar
        applicationContext.filesDir.listFiles().forEach {
            Log.d("deleteFiles", it.path)
            it.delete()
        }

        // Nota, esta sintaxis tambien borra la carpeta
        applicationContext.cacheDir.deleteRecursively()
    }

    fun querySpace(button: View) {
        // TODO 5.1 pedir al usuario que borre archivos para adquirir más espacio
        val intent = Intent().apply {
            action = StorageManager.ACTION_MANAGE_STORAGE
        }

        startActivity(intent)
    }
}