package com.smt.posesionista

import android.content.Context
import android.net.Uri
import androidx.core.content.FileProvider
import androidx.lifecycle.ViewModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.nio.charset.Charset
import kotlin.random.Random

class TableViewModel : ViewModel() {
    val inventory = mutableListOf<Thing>()
    val items = arrayOf("Book 1", "Book 2", "Map", "Compass", "Hiking shoes")
    private val gson = Gson()

    init {
        for (i in 0..99) {
            inventory.add(Thing(name=items[Random.nextInt(items.size)], price=Random.nextInt(10,200)))
        }
    }

    fun remove(pos: Int) = inventory.removeAt(pos)

    fun serialize(): String {
        return gson.toJson(inventory)
    }

    // TODO 3: Implementación de la escritura del contenido del modelo
    fun saveData(context: Context, uri: Uri?) {
        if (uri != null) {
            context.contentResolver.openOutputStream(uri).use {
                it?.write(this.serialize().toByteArray(Charset.defaultCharset()))
            }
        }
    }

    // TODO: Implementación de la lectura del contenido del modelo
    fun loadData(context: Context, uri: Uri?) {
        var jsonData: String? = null

        if (uri != null) {
            context.contentResolver.openInputStream(uri).use {
                jsonData = it?.readBytes()?.toString(Charset.defaultCharset())
            }

            if (jsonData != null) {
                val loadedInventory = gson.fromJson<List<Thing>>(jsonData, object : TypeToken<List<Thing>>() {}.type)

                inventory.clear()
                inventory.addAll(loadedInventory)
            }
        }
    }
}