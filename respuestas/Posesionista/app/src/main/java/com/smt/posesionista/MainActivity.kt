package com.smt.posesionista

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class MainActivity : AppCompatActivity(), TableFragment.Callback {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager.beginTransaction().apply {
            add(R.id.fragmentView, TableFragment.newInstance())
            commit()
        }

    }

    override fun onThingSelected(thing: Thing) {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragmentView, ThingFragment.newInstance(thing))
            addToBackStack(null)
            commit()
        }
    }
}
