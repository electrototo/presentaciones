package com.smt.posesionista

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.text.NumberFormat
import java.util.*

class TableFragment : Fragment() {

    interface Callback {
        fun onThingSelected(thing: Thing)
    }

    companion object {
        fun newInstance() = TableFragment()

        // TODO 2.1: Especificar el request code de la actividad de escritura
        const val saveFileRC = 1

        // TODO 4.1: Especificar el request code de la actividad de lectura
        const val readFileRC = 2
    }

    private var callback: Callback? = null
    private lateinit var viewModel: TableViewModel

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewAdapter: RecyclerView.Adapter<*>
    private lateinit var viewManager: RecyclerView.LayoutManager

    private lateinit var saveButton: Button
    private lateinit var loadButton: Button

    override fun onAttach(context: Context) {
        super.onAttach(context)
        callback = context as Callback?
    }

    override fun onDetach() {
        super.onDetach()
        callback = null
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.table_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        saveButton = view!!.findViewById(R.id.saveDataButton)
        saveButton.setOnClickListener {
            this.saveData(it)
        }

        loadButton = view!!.findViewById(R.id.loadDataButton)
        loadButton.setOnClickListener {
            this.loadData(it)
        }

        viewModel = ViewModelProvider(this).get(TableViewModel::class.java)

        viewManager = LinearLayoutManager(context)
        viewAdapter = ThingAdapter()

        recyclerView = view!!.findViewById<RecyclerView>(R.id.recyclerView).apply {
            layoutManager = viewManager
            adapter = viewAdapter
        }

        configTouchHelper()
    }

    fun saveData(view: View) {
        // TODO 2: Crear una actividad para seleccionar la ubicación del archivo
        val intent = Intent(Intent.ACTION_CREATE_DOCUMENT).apply {
            addCategory(Intent.CATEGORY_OPENABLE)
            type = "text/json"
            putExtra(Intent.EXTRA_TITLE, "posesionista.json")
        }

        startActivityForResult(intent, saveFileRC)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // TODO 2.2: Esperar el resultado de la actividad de escritura
        if (requestCode == saveFileRC && resultCode == Activity.RESULT_OK) {
            viewModel.saveData(context!!, data?.data)
        }

        // TODO 4.2: Esperar el resultado de la actividad de lectura
        if(requestCode == readFileRC && resultCode == Activity.RESULT_OK) {
            viewModel.loadData(context!!, data?.data)
            viewAdapter.notifyDataSetChanged()
        }
    }

    fun loadData(view: View) {
        // TODO 4: Crear una actividad para seleccionar la unicacion del archivo a leer
        val intent = Intent(Intent.ACTION_OPEN_DOCUMENT).apply {
            addCategory(Intent.CATEGORY_OPENABLE)
            type = "text/json"
        }

        startActivityForResult(intent, readFileRC)
    }

    private inner class ThingAdapter: RecyclerView.Adapter<ThingAdapter.ThingViewHolder>() {

        private inner class ThingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
            val thingName = itemView.findViewById<TextView>(R.id.thingName)
            val thingPrice = itemView.findViewById<TextView>(R.id.thingPrice)

            lateinit var thing: Thing

            init {
                itemView.setOnClickListener(this)
            }

            fun bind(thing: Thing) {
                this.thing = thing

                val format = NumberFormat.getInstance().apply {
                    maximumFractionDigits = 2
                    currency = Currency.getInstance("USD")
                }

                thingName.text = thing.name
                thingPrice.text = format.format(thing.price)
            }

            override fun onClick(view: View?) {
                callback?.onThingSelected(thing)
            }
        }

        override fun onCreateViewHolder(
            parent: ViewGroup,
            viewType: Int
        ): ThingAdapter.ThingViewHolder {
            val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.thing_item, parent, false)

            return ThingViewHolder(itemView)
        }

        override fun getItemCount() = viewModel.inventory.size

        override fun onBindViewHolder(holder: ThingAdapter.ThingViewHolder, position: Int) {
            val thing = viewModel.inventory[position]
            holder.bind(thing)
        }
    }
    private fun configTouchHelper() {
        val itemTouchCallback = object: ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                TODO("Not yet implemented")
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                if (direction == ItemTouchHelper.LEFT) {
                    viewModel.remove(viewHolder.adapterPosition)
                    recyclerView.adapter?.notifyItemRemoved(viewHolder.adapterPosition)
                }
            }
        }

        val gestureDetector = ItemTouchHelper(itemTouchCallback)
        gestureDetector.attachToRecyclerView(recyclerView)
    }
}