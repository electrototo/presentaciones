package com.smt.posesionista

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView


class ThingFragment(private val thing: Thing) : Fragment() {
    private lateinit var nameField: EditText
    private lateinit var priceField: EditText
    private lateinit var idField: EditText
    private lateinit var creationText: TextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_thing, container, false)

        nameField = view.findViewById(R.id.nameField)
        priceField = view.findViewById(R.id.priceField)
        idField = view.findViewById(R.id.idField)
        creationText = view.findViewById(R.id.creationText)

        nameField.setText(thing.name)
        priceField.setText(thing.price.toString())
        idField.setText(thing.id.toString())
        creationText.text = thing.creationDate.toString()

        return view
    }

    override fun onStart() {
        super.onStart()

        val watcher = object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
        }

        nameField.addTextChangedListener(watcher)
    }

    companion object {
        @JvmStatic
        fun newInstance(thing: Thing) = ThingFragment(thing)
    }
}